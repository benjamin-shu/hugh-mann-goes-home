var promptKey = "", prompted = false, options = {};

var gasManFlags = {
	disguise	: false,
	vegOil		: false,
	dirtyMag 	: false
};
function talkGasMan() {
	if (!gasManFlags.disguise) {
		gasManFlags.disguise = true;
		items.disguise = true;
		return "talk gasMan glasses";
	} else if (!gasManFlags.vegOil) {
		items.vegOil = true;
		gasManFlags.vegOil = true;
		return "talk gasMan vegOil";
	} else {
		if (items.dirtyMag && !gasManFlags.dirtyMag) {
			items.tools = true;
			ufoFlags.tools = true;
			gasManFlags.dirtyMag = true;
			return "talk gasMan dirtyMag";
		} else { return "talk gasMan"; }
	}
}

var momFlags = {
	sus			: 0
};
function talkMom() {
	if (!items.disguise){
		momFlags.sus += 20;
		if (momFlags.sus > sus) {
			sus = momFlags.sus;
			susMeter.innerHTML = sus + "%";
		}
		return ("talk mom sus " + momFlags.sus);
	} else { return "talk mom"; }
}

var kidFlags = {
	sus 		: 0,
	kite		: false,
	dirtyMag	: false
};
function talkKid() {
	console.log("talk kid");
	if (!items.disguise) {
		kidFlags.sus += 20;
		if (kidFlags.sus > sus) {
			sus = kidFlags.sus;
			susMeter.innerHTML = sus + "%";
		}
		return ("talk kid sus " + kidFlags.sus);
	} else {
		if (!items.kite) {
			return "talk kid help";
		} else if (!kidFlags.kite) {
			kidFlags.kite = true;
			kidFlags.dirtyMag = true;
			items.dirtyMag = true;
			if (treeFlags.usedJump) { return "talk kid kite jump"; }
			else if (treeFlags.usedRock) { return "talk kid kite rock"; }
		} else { return "talk kid thanks"; }
	}
}

var teacherFlags = {
	zodiac		: false,
	rock		: false,
	starMap		: false,
	sus			: 0
};

var zodiacSigns =["aries", 
"taurus", "gemini", "cancer", 
"leo", "virgo", "libra", 
"scorpio", "sagittarius", "capricorn"
, "aquarius", "pisces"];

function talkTeacher() {
	console.log("talk teacher");
	if (!items.disguise) {
		console.log("Talking to teacher without disguise");
		teacherFlags.sus += 20;
		if (teacherFlags.sus > sus) {
			sus = teacherFlags.sus;
			susMeter.innerHTML = sus + "%";
		}
		return ("talk teacher sus " + teacherFlags.sus);
	} else {
		if ( !teacherFlags.rock && !teacherFlags.zodiac) {
			console.log("Talking to teacher without rock");
			prompted = true;
			promptKey = "talk teacher sign fail";
			options = { "aries" : zodiacRight, "taurus" : zodiacRight, 
			"gemini" : zodiacRight, "cancer" : zodiacRight, 
			"leo" : zodiacRight, "virgo" : zodiacRight, 
			"libra" : zodiacRight, "scorpio" : zodiacRight, 
			"sagittarius" : zodiacRight, "capricorn" : zodiacRight, 
			"aquarius" : zodiacRight, "pices" : zodiacRight,
			"Aries" : zodiacRight, "Taurus" : zodiacRight, 
			"Gemini" : zodiacRight, "Cancer" : zodiacRight, 
			"Leo" : zodiacRight, "Virgo" : zodiacRight, 
			"Libra" : zodiacRight, "Scorpio" : zodiacRight, 
			"Sagittarius" : zodiacRight, "Capricorn" : zodiacRight, 
			"Aquarius" : zodiacRight, "Pices" : zodiacRight,  
			"nothing" : doNothing };
			// teacherFlags.sus += 20;
			if (teacherFlags.sus > sus) {
				sus = teacherFlags.sus;
				susMeter.innerHTML = sus + "%";
			}
			return "talk teacher";
		}
		else if (items.rock && !teacherFlags.rock && teacherFlags.zodiac) {
			console.log("Talking to teacher with rock");
			teacherFlags.rock = true;
			teacherFlags.starMap = true;
			items.starMap = true;
			return "talk teacher give rock";
		} 
		else {
			console.log("Talking to teacher passed zodiac");
			prompted = false;
			return "talk teacher thanks";
		}
	}
}

function zodiacRight() {
	console.log("zodiac correct");
	if(sus >= 50){
		sus = sus-20;
		susMeter.innerHTML = sus + "%";}
		teacherFlags.zodiac = 'gemini';
	setTimeout(function() {
		prompted = false;
		options = {};
		promptKey = "";
	}, 1000);
	return "talk teacher sign pass";
}

var fastFoodGuyFlags = {
	sus			: 0,
	sample 		: false,
	foodEaten	: false,
	vegOil		: false
};
function talkFastFoodGuy() {
	console.log("talk fastfoodguy");
	if (!items.disguise) {
		fastFoodGuyFlags.sus += 20;
		if (fastFoodGuyFlags.sus > sus) {
			sus = fastFoodGuyFlags.sus;
			susMeter.innerHTML = sus + "%";
		}
		return ("talk fastfoodguy sus " + fastFoodGuyFlags.sus);
	} else {
		if (!fastFoodGuyFlags.sample) {
			prompted = true;
			options = { "yes" : fastFoodYesSample, "no" : fastFoodNoSample };
			promptKey = "talk fastfoodguy sample prompt";
			return "talk fastfoodguy sample";
		} else if (!items.vegOil && !fastFoodGuyFlags.vegOil) {
			return "talk fastfoodguy vegOil";
		} else if (items.vegOil && !fastFoodGuyFlags.vegOil) {
			fastFoodGuyFlags.vegOil = true;
			items.radOil = true;
			return "talk fastfoodguy vegOil trade";
		} else { return "talk fastfoodguy thanks"; }
	}
}
function fastFoodYesSample() {
	fastFoodGuyFlags.sample = true;
	prompted = false;
	options = {};
	promptKey = "";
	
	fastFoodGuyFlags.sus += 20;
	if (fastFoodGuyFlags.sus > sus) {
		sus = fastFoodGuyFlags.sus;
		susMeter.innerHTML = sus + "%";
	}
	return "fastfoodguy sample yes";
}
function fastFoodNoSample() {
	fastFoodGuyFlags.sample = true;
	prompted = false;
	options = {};
	promptKey = "";
	return "fastfoodguy sample no";
}

var ufoFlags = {
	tools		: false,
	radOil 		: false,
	starMap		: false
};
function lookUFO() {
	console.log("looking at UFO");
	if (items.tools){ ufoFlags.tools = true; document.getElementById("gotTools").style = ""; }
	if (items.radOil) { ufoFlags.radOil = true; document.getElementById("gotFuel").style = ""; }
	if (items.starMap) { ufoFlags.starMap = true; document.getElementById("gotMap").style = ""; }
	if (ufoFlags.tools && ufoFlags.radOil && ufoFlags.starMap) {
		/* Game is won here! */
		gameWon = true;
	}
	return "look ufo parts";
}

var rockFlags = {
	pickedUp : false
};
function lookRock() {
	if (!rockFlags.pickedUp) {
		rockFlags.pickedUp = true;
		items.rock = true;
		return "look rock";
	} else { return "look rock pickedUp"; }
}

var treeFlags = {
	gotKite		: false,
	usedJump	: false,
	usedRock	: false
};
function lookTree() {
	if (!treeFlags.gotKite && currentTile != null && currentTile.x == 28) {
		options = { "jump" : kiteJump, "nothing" : doNothing };
		if (items.rock) { document.getElementById("kite rock").style = ""; options["rock"] = kiteRock; }
		prompted = true;
		return "look tree kite";
	} else { return "look tree"; }
}
function kiteRock() {
	console.log("rock");
	kite.setVelocityY(10);
	setTimeout(function() {
		treeFlags.usedRock = true;
		
		prompted = false;
		options = {};
		promptKey = "";
	}, 1000);
	return "home";
}
function kiteJump() {
	console.log("jump");
	player.setVelocityY(-30);
	player.setAccelerationY(10);
	setTimeout(function() {
		treeFlags.usedJump = true;

		prompted = false;
		options = {};
		promptKey = "";
	}, 1000);
	return "home";
}
function getKite(player, kite) {
	console.log("got Kite!");
	kite.disableBody(true, true);
	kite.destroy();
	items.kite = true;
}

function doNothing() {
	console.log("nothing");
	prompted = false;
	options = {};
	promptKey = "";
	return "home";
}

var sectionArray;
var talkTargets = [ "gasMan", "mom", "kid", "teacher", "fastfoodguy" ]
	talkHandlers = {
		"gasMan"		: talkGasMan,
		"mom"			: talkMom,
		"kid"			: talkKid,
		"teacher"		: talkTeacher,
		"fastfoodguy"	: talkFastFoodGuy
	};
var lookTargets = [ "ufo", "rock", "tree" ],
	lookHandlers = {
		"ufo"			: lookUFO,
		"rock"			: lookRock,
		"tree"			: lookTree
	};

/*!
 * jQuery CLI
 * Simulating a command line interface with jQuery
 *
 * @version : 1.0.0
 * @author : Paulo Nunes (http://syndicatefx.com)
 * @demo : https://codepen.io/syndicatefx/pen/jPxXpz
 * @license: MIT
 */

/*!* 
 * jQuery Text Typer plugin
 * https://github.com/gr8pathik/jquery-texttyper
*/

(function(e){"use strict";e.fn.textTyper=function(t){var n={typingClass:"typing",beforeAnimation:function(){},afterAnimation:function(){},speed:5,nextLineDelay:400,startsFrom:0,repeatAnimation:false,repeatDelay:4e3,repeatTimes:1,cursorHtml:'<span class="cursor">|</span>'},r=e.extend({},n,t);this.each(function(){var t=e(this),n=1,i="typingCursor";var s=t,o=s.length,u=[];while(o--){u[o]=e.trim(e(s[o]).html());e(s[o]).html("")}t.init=function(e){var n=r.beforeAnimation;if(n)n();t.animate(0)};t.animate=function(o){var a=s[o],f=r.typingClass,l=r.startsFrom;e(a).addClass(f);var c=setInterval(function(){var f=r.cursorHtml;f=e("<div>").append(e(f).addClass(i)).html();e(a).html(u[o].substr(0,l)+f);l++;if(u[o].length<l){clearInterval(c);o++;if(s[o]){setTimeout(function(){e(a).html(u[o-1]);t.animate(o)},r.nextLineDelay)}else{e(a).find("."+i).remove();if(r.repeatAnimation&&(r.repeatTimes==0||n<r.repeatTimes)){setTimeout(function(){t.animate(0);n++},r.repeatDelay)}else{var h=r.afterAnimation;if(h)h()}}}},r.speed)};t.init()});return this}})(jQuery)

$(document).ready(function() {
	$('.command').hide();
	$('input[type="text"]').focus();
	$('#home').addClass('open');
	$('#home').textTyper({
		speed:5,
		afterAnimation:function(){
			$('.command').fadeIn();
			$('input[type="text"]').focus();
			$('input[type="text"]').val('');
		}
	});

	// get array of section ids, that exist in DOM
	sectionArray = [];
	// We are using <section> here, you can use <div> or <article> if you want
	$('section').each( function(i,e) {
		//you can use e.id instead of $(e).attr('id')
		sectionArray.push($(e).attr('id'));
	});

	// Debug
	console.log(sectionArray);

	// Command Input------------------------------

	$('input[type="text"]').keyup(function(e){
		if(e.which == 13){// ENTER key pressed
			$('.command').hide();
			var destination = $('input[type="text"]').val().trim();

			/* ----------------------------------------------------- */
			/* Modifications/additions made for Hugh Mann Goes Home. */
			/* @author Benjamin Shu                                  */
			/* ----------------------------------------------------- */

			if (!prompted) {
				if (destination != "") {
					if (destination != "home" && destination != "help") {
						var command = destination.substring(0, 4), target = destination.substring(5).trim();
						console.log("command: " + command + "\ntarget: " + target);
						if (command == "talk") {
							console.log("talking");
							/* Check that target is an NPC and not an object. */
							if (talkTargets.indexOf(target) >= 0) {
								destination = talkHandlers[target]();
								console.log("destination: " + destination);
							} else { console.log("err"); destination = "error"; }
						} else if (command == "look") {
							console.log("looking");
							if (lookTargets.indexOf(target) >= 0) {
								destination = lookHandlers[target]();
								console.log("destination: " + destination);
							}
						} else { console.log("invalid command"); destination = "error"; }
		
						if (sus >= 100) { gameOver = true; return; }
					}
				} else {
					destination = "home";
				}
			} else {
				if (destination != "" && options[destination]) {
					destination = options[destination]();
				} else {
					destination = promptKey;
					sus += 20;
					if (sus >= 100) { gameOver = true; return; }
					susMeter.innerHTML = sus + " %";
				}
			}
			console.log(destination);

			/* ----------------------------------------------------- */
			/* End of modifications/additions.                       */
			/* ----------------------------------------------------- */

			// Display section with id == destination and hide all others
			$('section[id="' + destination + '"]').addClass('open').siblings().removeClass('open');

			// If destination does not match our array of section ids, display error section
			if($.inArray(destination, sectionArray) == -1){
				console.log("not found");
				$('#error').addClass('open');
				$('#error').siblings().removeClass('open');
			}
		
			// All sections with class .open init textTyper
			$('.open').textTyper({
				speed:5,
				afterAnimation:function(){
					$('.command').fadeIn();
					$('input[type="text"]').focus();
					$('input[type="text"]').val('');
				}
			});
		}// end if ENTER key pressed
	});// end keyup function
// End Command Input-----------------------------
});