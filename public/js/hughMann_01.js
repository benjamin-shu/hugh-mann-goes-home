/* Create Phaser configuration settings. */
var config;

var player, disguise = false;
var currentTile, moved = false;
var cursors;

/*Names for npcs and movable objects*/
var gasMan;
var mom;
var kid;
var teacher;
var kite;
/*Array to hold npcs and movable objects*/
var npcs = [];

var tileMaps = [];
var map, mapIndex, tiles, details, interactives, background;
var layerBackground, layerDetails, layerInteractives, layerPlatforms;
const gameWidth = 1024, gameHeight = 576;
var screenObjects, screenPeople;

var camera, controls;
var susMeter, sus = 0;
var gameOver = false, stopped = false;

const tileWidth = 32;
const tileHeight = 32;

var velocity = 128;
const minSpeed = 128;
const maxSpeed = 640;

var game;
var commandLine;

var splashStart = false;
var splashScreen, menuScreen;
var startButton, controlsButton, helpButton;

var wheelSound;

function preload() {
    /* Load background image. */
    this.load.image('background', 'resources/textures/background.png');
    this.load.image('splashScreen', 'resources/images/Splash_Resize.png');

    /* Load sprite sheets for Hugh Mann. */
    this.load.spritesheet('hughMann-idle', 'resources/textures/hugh-idle-sheet.png', { frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('hughMann-idle-disguise', 'resources/textures/hugh-idle-disguise-sheet.png', { frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('hughMann-walking-left', 'resources/textures/hugh-walking-left-sheet.png', { frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('hughMann-walking-right', 'resources/textures/hugh-walking-right-sheet.png', { frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('hughMann-walking-left-disguise', 'resources/textures/hugh-walking-left-disguise-sheet.png', { frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('hughMann-walking-right-disguise', 'resources/textures/hugh-walking-right-disguise-sheet.png', { frameWidth: 32, frameHeight: 64 });

    /* Load sprite sheets for NPCS */
    this.load.spritesheet('gas-man','resources/textures/Station-attendant-sheet.png',{frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('gas-man-disguise','resources/textures/Station-attendant-sheet-with-disguise.png',{frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('mom','resources/textures/mom-sheet.png',{frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('kid','resources/textures/kid-sheet.png',{frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('teacher','resources/textures/teacher-sheet.png',{frameWidth: 32, frameHeight: 64 });
    console.log("Loaded all npc resources");

    /*Load sprite sheets for moving objects*/
    this.load.spritesheet('kite','resources/textures/kite.png',{frameWidth: 32, frameHeight: 64 });

    /* Load tileset images for the main three map layers. */
    this.load.image('maptiles', 'resources/textures/maptiles.png');
    this.load.image('detailImage', 'resources/textures/details.png');
    this.load.image('interactivesImage', 'resources/textures/interactives.png');

    /*Load map info*/
    tileMaps.push(this.load.tilemapTiledJSON('map1', 'resources/maps/map1.json'));
    tileMaps.push(this.load.tilemapTiledJSON('map2', 'resources/maps/map2.json'));
    tileMaps.push(this.load.tilemapTiledJSON('map3', 'resources/maps/map3.json'));
    tileMaps.push(this.load.tilemapTiledJSON('map4', 'resources/maps/map4.json'));
    mapIndex = 0;

    /* Load music */
    this.load.audio('firstAreaMusic','resources/audio/startingMusic.mp3');

    /* Load sound effect */
    this.load.audio('metalImpact', 'resources/audio/metalImpact.mp3');
    this.load.audio('wheelSound', 'resources/audio/wheelSound.mp3');
    this.load.audio('objectInteractionSound', 'resources/audio/objectInteractionSound.mp3');
};

function create() {
    /* Start music */
    var music = this.sound.add('firstAreaMusic');
    /* Configure music to loop */
    music.setLoop(true);
    music.play();
    /* Load background image. */
    this.add.image(1024, 270,'background');

    /* Create new tilemap object. */
    map = this.make.tilemap({ key: 'map1'});

    /*Create tilesets*/
    detail = map.addTilesetImage('details', 'detailImage');
    tiles = map.addTilesetImage('maptiles','maptiles');
    interactives = map.addTilesetImage('interactives', 'interactivesImage');

    /* Add visual details to the tilemap. */
    layerDetails = map.createStaticLayer('detail', detail, 0, 0);

    /* Add interactive objects to the tilemap. */
    interactiveObjects = new Array();
    layerInteractives = map.createStaticLayer('interactives', interactives, 0, 0);

    /* Apply tileset.png to the map. */
    layerPlatforms = map.createStaticLayer('platforms', tiles, 0, 0);
    layerPlatforms.setCollisionByProperty({ collides: true });
    
    /* Add Hugh Mann sprite for player to use. */
    player = this.physics.add.sprite(256, 480, 'hughMann-idle-disguise');
    player.setCollideWorldBounds(true);
    this.physics.add.collider(player, layerPlatforms);
    
    /* Add animations to Hugh Mann sprite. */
    this.anims.create({
        key: 'left',
        frames: this.anims.generateFrameNumbers('hughMann-walking-left', { start: 0, end: 6 }),
        frameRate: 16,
        repeat: 0
    });

    this.anims.create({
        key: 'left-disguise',
        frames: this.anims.generateFrameNumbers('hughMann-walking-left-disguise', { start: 0, end: 6 }),
        frameRate: 16,
        repeat: 0
    });

    this.anims.create({
        key: 'right',
        frames: this.anims.generateFrameNumbers('hughMann-walking-right', { start: 0, end: 6 }),
        frameRate: 16,
        repeat: 0
    });

    this.anims.create({
        key: 'right-disguise',
        frames: this.anims.generateFrameNumbers('hughMann-walking-right-disguise', { start: 0, end: 6 }),
        frameRate: 16,
        repeat: 0
    });

    this.anims.create({
        key: 'idle',
        frames: this.anims.generateFrameNumbers('hughMann-idle', { start: 0, end: 26 }),
        frameRate: 4,
        repeat: -1
    });

    this.anims.create({
        key: 'idle-disguise',
        frames: this.anims.generateFrameNumbers('hughMann-idle-disguise', { start: 0, end: 26 }),
        frameRate: 4,
        repeat: -1
    });

    /* Add animations for NPCS */
    //this.load.spritesheet('gas-man','resources/textures/Station-attendant-sheet.png/',{frameWidth: 32, frameHeight: 64 });
    //this.load.spritesheet('gas-man-disguise','resources/textures/Station-attendant-sheet-with-disguise.png/',{frameWidth: 32, frameHeight: 64 });
    //this.load.spritesheet('mom','resources/textures/mom-sheet.png/',{frameWidth: 32, frameHeight: 64 });
    //this.load.spritesheet('kid','resources/textures/kid-sheet.png/',{frameWidth: 32, frameHeight: 64 });
    this.anims.create({
        key: 'idle-station-attendant',
        frames: this.anims.generateFrameNumbers('gas-man', { start: 0, end: 16 }),
        frameRate: 4,
        repeat: -1
    });
    this.anims.create({
        key: 'idle-station-attendant-disguise',
        frames: this.anims.generateFrameNumbers('gas-man-disguise', { start: 0, end: 16 }),
        frameRate: 4,
        repeat: -1
    });
    this.anims.create({
        key: 'idle-mom',
        frames: this.anims.generateFrameNumbers('mom', { start: 0, end: 16 }),
        frameRate: 6,
        repeat: -1
    });
    this.anims.create({
        key: 'idle-kid',
        frames: this.anims.generateFrameNumbers('kid', { start: 0, end: 16 }),
        frameRate: 7,
        repeat: -1
    });
    this.anims.create({
        key: 'idle-teacher',
        frames: this.anims.generateFrameNumbers('teacher', { start: 0, end: 16 }),
        frameRate: 7,
        repeat: -1
    });


    /* Set up to detect keyboard input. */
    cursors = this.input.keyboard.addKeys({
        left:   Phaser.Input.Keyboard.KeyCodes.LEFT,
        right:  Phaser.Input.Keyboard.KeyCodes.RIGHT,
        enter:  Phaser.Input.Keyboard.KeyCodes.ENTER,
        key1:   Phaser.Input.Keyboard.KeyCodes.ONE,
        key2:   Phaser.Input.Keyboard.KeyCodes.TWO,
        key3:   Phaser.Input.Keyboard.KeyCodes.THREE
    });

    camera = this.cameras.main;
    camera.width = 1024;
    camera.height = 576;
    camera.setBounds(0, 0, 1024, 576);
    camera.scrollX = 0; camera.scrollY = 0;
    camera.startFollow(player);

    // menuScreen = this.add.image(512, 288, 'menuScreen');
    // startButton = this.add.text(512, 150, "Start Game", { fill: '#000' });
    // startButton.setInteractive();
    // controlsButton = this.add.text(512, 150, "Controls", { fill: '#000' });
    // controlsButton.setInteractive();
    // helpButton = this.add.text(512, 150, "Controls", { fill: '#000' });
    // helpButton.setInteractive();

    splashScreen = this.add.image(512, 288, 'splashScreen');

    // Add sound effect
    wheelSound = this.sound.add('wheelSound', {loop: true, volume: 0.2});
    this.sound.add('objectInteractionSound');
};

function update(){
    if (!splashStart) {
        if (cursors.enter.isDown) {
            splashStart = true;
            splashScreen.destroy();
        }
    } else if (gameOver) {
        if (stopped) { return; }
        else {
            stopped = true;
            this.add.text(450, 190, 'GAME\nOVER', { fontSize: '100px', fill: '#000' });
        }
    } else {
        if (cursors.left.isDown) {
            if (player.x == 16 && mapIndex > 0) {
                mapIndex--;
                map.destroy();
                if (mapIndex == 0) {
                    /*Destroy sprites not in map*/
                    while(npcs.length > 0){
                        var temp = npcs.pop();
                        temp.destroy();
                    }
                    /* Create new tilemap object. */
                    map = this.make.tilemap({ key: 'map1'});
                    
                     /*Create tilesets*/
                    detail = map.addTilesetImage('details', 'detailImage');
                    tiles = map.addTilesetImage('maptiles','maptiles');
                    interactives = map.addTilesetImage('interactives', 'interactivesImage');

                    /* Add visual details to the tilemap. */
                    layerDetails = map.createStaticLayer('detail', details, 0, 0);

                    /* Add interactive objects to the tilemap. */
                    interactiveObjects = new Array();
                    layerInteractives = map.createStaticLayer('interactives', interactives, 0, 0);

                    /* Apply tileset.png to the map. */
                    layerPlatforms = map.createStaticLayer('platforms', tiles, 0, 0);
                    layerPlatforms.setCollisionByProperty({ collides: true });

                    player.destroy();
                    player = this.physics.add.sprite(gameWidth - 17, 480, disguise ? 'hughMann-idle-disguise' : 'hughMann-idle');
                    player.setCollideWorldBounds(true);
                    this.physics.add.collider(player, layerPlatforms);
                }
                else if (mapIndex == 1) {
                    /*Destroy sprites not in map*/
                    while(npcs.length > 0){
                        var temp = npcs.pop();
                        temp.destroy();
                    }
                    /* Create new tilemap object. */
                    map = this.make.tilemap({ key: 'map2'});
                    
                     /*Create tilesets*/
                    details = map.addTilesetImage('details', 'detailImage');
                    tiles = map.addTilesetImage('maptiles','maptiles');
                    interactives = map.addTilesetImage('interactives', 'interactivesImage');

                    /* Add visual details to the tilemap. */
                    layerDetails = map.createStaticLayer('detail', details, 0, 0);

                    /* Add interactive objects to the tilemap. */
                    interactiveObjects = new Array();
                    layerInteractives = map.createStaticLayer('interactives', interactives, 0, 0);

                    /* Apply tileset.png to the map. */
                    layerPlatforms = map.createStaticLayer('platforms', tiles, 0, 0);
                    layerPlatforms.setCollisionByProperty({ collides: true });

                    /* Add sprites to the tilemap */
                    gasMan = this.add.sprite(900, 480, 'gas-man');
                    gasMan.name = "gasMan";
                    gasMan.play('idle-station-attendant-disguise');
                    npcs.push(gasMan);
                    
                    player.destroy();
                    player = this.physics.add.sprite(gameWidth - 17, 480, disguise ? 'hughMann-idle-disguise' : 'hughMann-idle');
                    player.setCollideWorldBounds(true);
                    this.physics.add.collider(player, layerPlatforms);
                }
            }
            player.setVelocityX(velocity * -1);
            player.anims.play(disguise ? 'left-disguise' : 'left', true);
            moved = true;
        } else if (cursors.right.isDown) {
            if (player.x == 1008 && mapIndex < tileMaps.length - 1) {
                mapIndex++;
                map.destroy();
                //window.alert("Map index is: "+mapIndex);
                if (mapIndex == 1) {
                     /*Destroy sprites not in map*/
                    while(npcs.length > 0){
                        var temp = npcs.pop();
                        temp.destroy();
                    }
                    /* Create new tilemap object. */
                    map = this.make.tilemap({ key: 'map2'});
                    console.log('Created map map 2');

                    /*Create tilesets*/
                    tiles = map.addTilesetImage('maptiles','maptiles');
                    
                    /* Apply tileset.png to the map. */
                    layerPlatforms = map.createStaticLayer('platforms', tiles);
                    layerPlatforms.setCollisionByProperty({ collides: true });
                    
                    details = map.addTilesetImage('details', 'detailImage');
                    interactives = map.addTilesetImage('interactives', 'interactivesImage');
                    
                    /* Add visual details to the tilemap. */
                    layerDetails = map.createStaticLayer('detail', details);

                    /* Add interactive objects to the tilemap. */
                    interactiveObjects = new Array();
                    layerInteractives = map.createStaticLayer('interactives', interactives);
                    
                    /* Add sprites to the tilemap */
                    gasMan = this.add.sprite(900, 480, 'gas-man');
                    gasMan.name = "gasMan";
                    gasMan.play('idle-station-attendant-disguise');
                    npcs.push(gasMan);

                    player.destroy();
                    player = this.physics.add.sprite(17, 480, disguise ? 'hughMann-idle-disguise' : 'hughMann-idle');
                    player.setCollideWorldBounds(true);
                    this.physics.add.collider(player, layerPlatforms);
                }
                else if (mapIndex == 2) {
                    /*Destroy sprites not in map*/
                    while(npcs.length > 0){
                        var temp = npcs.pop();
                        temp.destroy();
                    }
                    
                    /* Create new tilemap object. */
                    map = this.make.tilemap({ key: 'map3'});
                    console.log(map);
                    console.log(this);

                    /*Create tilesets*/
                    tiles = map.addTilesetImage('maptiles','maptiles');
                    console.log(tiles);
                    
                    /* Apply tileset.png to the map. */
                    layerPlatforms = map.createStaticLayer('platforms', tiles);
                    layerPlatforms.setCollisionByProperty({ collides: true });
                    
                    /* Add visual details to the tilemap. */
                    details = map.addTilesetImage('details', 'detailImage');
                    console.log(details);
                    layerDetails = map.createStaticLayer('detail', details, 0, 0);
                    console.log(layerDetails);

                    /* Add interactive objects to the tilemap. */
                    interactives = map.addTilesetImage('interactives', 'interactivesImage');
                    console.log(interactives);
                    layerInteractives = map.createStaticLayer('interactives', interactives, 0, 0);
                    console.log(layerInteractives);

                    /* Add sprites to the tilemap */
                    mom = this.add.sprite(800, 480, 'mom');
                    mom.name = "mom";
                    mom.play('idle-mom');
                    npcs.push(mom);

                    kid = this.add.sprite(900,480,'kid');
                    kid.name = "kid";
                    kid.play('idle-kid');
                    npcs.push(kid);

                    kite = this.add.sprite(920,420,'kite');
                    npcs.push(kite);
                    
                    /*Destroy sprites not in the map*/
                    if(gasMan!=undefined){
                        gasMan.destroy();
                        console.log("Destroyed sprite");
                    }

                    player.destroy();
                    player = this.physics.add.sprite(17, 480, disguise ? 'hughMann-idle-disguise' : 'hughMann-idle');
                    player.setCollideWorldBounds(true);
                    this.physics.add.collider(player, layerPlatforms);
                }
                else if (mapIndex == 3) {
                    //window.alert('Map index is ' = mapIndex);
                    /*Destroy sprites not in map*/
                    while(npcs.length > 0){
                        var temp = npcs.pop();
                        temp.destroy();
                    }
                    
                    /* Create new tilemap object. */
                    map = this.make.tilemap({ key: 'map4'});
                    console.log(map);
                    console.log(this);

                    /*Create tilesets*/
                    tiles = map.addTilesetImage('maptiles','maptiles');
                    console.log(tiles);
                    
                    /* Apply tileset.png to the map. */
                    layerPlatforms = map.createStaticLayer('platforms', tiles);
                    layerPlatforms.setCollisionByProperty({ collides: true });
                    
                    /* Add visual details to the tilemap. */
                    details = map.addTilesetImage('details', 'detailImage');
                    console.log(details);
                    layerDetails = map.createStaticLayer('detail', details, 0, 0);
                    console.log(layerDetails);

                    /* Add interactive objects to the tilemap. */
                    interactives = map.addTilesetImage('interactives', 'interactivesImage');
                    console.log(interactives);
                    layerInteractives = map.createStaticLayer('interactives', interactives, 0, 0);
                    console.log(layerInteractives);

                    /* Add sprites to the tilemap */
                    
                    /*Destroy sprites not in the map*/
                    if(gasMan!=undefined){
                        gasMan.destroy();
                        console.log("Destroyed sprite");
                    }

                    player.destroy();
                    player = this.physics.add.sprite(17, 480, disguise ? 'hughMann-idle-disguise' : 'hughMann-idle');
                    player.setCollideWorldBounds(true);
                    this.physics.add.collider(player, layerPlatforms);
                }
            }
            player.setVelocityX(velocity * 1);
            player.anims.play(disguise ? 'right-disguise' : 'right', true);
            moved = true;
        } else {
            player.setVelocityX(0);
            player.anims.play(disguise ? 'idle-disguise' : 'idle', true);
            if (moved) {
                currentTile = map.getTileAt(Math.floor(player.x / 32), Math.floor((player.y + 20) / 32), false, layerInteractives);
                console.log(currentTile);
                console.log("Clearing list!");
                while (screenObjects.firstChild) {
                    screenObjects.firstChild.remove();
                }
                $('#objects').empty();
                if (currentTile != null && currentTile.properties.interactive) {
                    console.log(currentTile.properties.name);
                    var node = document.createElement('li');
                    var text = document.createTextNode(currentTile.properties.name);
                    node.appendChild(text);
                    screenObjects.appendChild(node);
                    $('#objects').append(node);
                }
                console.log(screenObjects);
                
                while (screenPeople.firstChild) {
                    screenPeople.firstChild.remove();
                }
                $('#people').empty();
                var i = 0, bounds = player.getBounds();
                for (i = 0; i < npcs.length; i++) {
                    if (Phaser.Geom.Intersects.RectangleToRectangle(bounds, npcs[i].getBounds())) {
                        var node = document.createElement('li');
                        var text = document.createTextNode(npcs[i].name);
                        node.appendChild(text);
                        screenPeople.appendChild(node);
                        $('#people').append(node);
                    }
                }
                console.log(screenPeople);
                moved = false;
            }
        }
        if (cursors.key1.isDown) {
            /* Teleport to Area 1 - Tutorial. */
            /*Destroy sprites not in map*/
            while(npcs.length > 0){
                var temp = npcs.pop();
                temp.destroy();
            }
                        
            map.destroy();
            mapIndex = 0;

            /* Create new tilemap object. */
            map = this.make.tilemap({ key: 'map1'});
            
            /*Create tilesets*/
            details = map.addTilesetImage('details', 'detailImage');
            tiles = map.addTilesetImage('maptiles','maptiles');
            interactives = map.addTilesetImage('interactives', 'interactivesImage');

            /* Add visual details to the tilemap. */
            layerDetails = map.createStaticLayer('detail', details, 0, 0);

            /* Add interactive objects to the tilemap. */
            interactiveObjects = new Array();
            layerInteractives = map.createStaticLayer('interactives', interactives, 0, 0);

            /* Apply tileset.png to the map. */
            layerPlatforms = map.createStaticLayer('platforms', tiles, 0, 0);
            layerPlatforms.setCollisionByProperty({ collides: true });

            player.destroy();
            player = this.physics.add.sprite(512, 480, disguise ? 'hughMann-idle-disguise' : 'hughMann-idle');
            player.setCollideWorldBounds(true);
            this.physics.add.collider(player, layerPlatforms);
        } else if (cursors.key2.isDown) {
            /* Teleport to Area 2 - Gas Station. */
            /*Destroy sprites not in map*/
            while(npcs.length > 0){
                var temp = npcs.pop();
                temp.destroy();
            }
            map.destroy();
            mapIndex = 1;

            /* Create new tilemap. */
            map = this.make.tilemap({ key: 'map2'});
            console.log('Created map map 2')

            /*Create tilesets*/
            tiles = map.addTilesetImage('maptiles','maptiles');
            
            /* Apply tileset.png to the map. */
            layerPlatforms = map.createStaticLayer('platforms', tiles);
            layerPlatforms.setCollisionByProperty({ collides: true });
            
            /* Add visual details to the tilemap. */
            details = map.addTilesetImage('details', 'detailImage');
            layerDetails = map.createStaticLayer('detail', details);

            /* Add interactive objects to the tilemap. */
            interactives = map.addTilesetImage('interactives', 'interactivesImage');
            layerInteractives = map.createStaticLayer('interactives', interactives,0,0);

            /* Add sprites to the tilemap */
            gasMan = this.add.sprite(900, 480, 'gas-man');
            gasMan.name = "gasMan";
            gasMan.play('idle-station-attendant-disguise');
            npcs.push(gasMan);

            player.destroy();
            player = this.physics.add.sprite(512, 480, disguise ? 'hughMann-idle-disguise' : 'hughMann-idle');
            player.setCollideWorldBounds(true);
            this.physics.add.collider(player, layerPlatforms);

        } else if (cursors.key3.isDown) {
            /* Teleport to Area 3 - Park. */
            /*Destroy sprites not in the map*/
            /*Destroy sprites not in map*/
            while(npcs.length > 0){
                var temp = npcs.pop();
                temp.destroy();
            }
            map.destroy();

            mapIndex = 2;

            map = this.make.tilemap({ key: 'map3'});
            console.log(map);
            console.log(this);

            /*Create tilesets*/
            tiles = map.addTilesetImage('maptiles','maptiles');
            console.log(tiles);
            
            /* Apply tileset.png to the map. */
            layerPlatforms = map.createStaticLayer('platforms', tiles);
            layerPlatforms.setCollisionByProperty({ collides: true });
            
            /* Add visual details to the tilemap. */
            details = map.addTilesetImage('details', 'detailImage');
            console.log(details);
            layerDetails = map.createStaticLayer('detail', details, 0, 0);
            console.log(layerDetails);

            /* Add interactive objects to the tilemap. */
            interactives = map.addTilesetImage('interactives', 'interactivesImage');
            console.log(interactives);
            layerInteractives = map.createStaticLayer('interactives', interactives, 0, 0);
            console.log(layerInteractives);

            /* Add sprites to the tilemap */
            mom = this.add.sprite(800, 480, 'mom');
            mom.name = "mom";
            mom.play('idle-mom');
            npcs.push(mom);

            kid = this.add.sprite(900,480,'kid');
            kid.name = "kid";
            kid.play('idle-kid');
            npcs.push(kid);

            kite = this.add.sprite(920,420,'kite');
            npcs.push(kite);

            player.destroy();
            player = this.physics.add.sprite(512, 480, disguise ? 'hughMann-idle-disguise' : 'hughMann-idle');
            player.setCollideWorldBounds(true);
            this.physics.add.collider(player, layerPlatforms);
        }
    }
    if (player.body.speed > 0 && splashStart == true) {
        if (wheelSound.isPlaying == false) {
            wheelSound.play();
        }
    } else {
        wheelSound.stop();
    }
    
};

/* Create new game object. */
window.onload = function() {
    commandLine     = document.getElementById("commandLine");
    screenObjects   = document.getElementById("objects");
    screenPeople    = document.getElementById("people");
    susMeter        = document.getElementById("suspicion");
    susMeter.innerHTML = sus + " %";

    config = {
        type: Phaser.CANVAS,
        width: gameWidth,
        height: gameHeight,
        physics: {
            default: 'arcade',
            arcade: {
                gravity: { y: 10 },
                debug: false
            }
        },
        scene: {
            preload: preload,
            create: create,
            update: update
        },
        canvas: document.getElementById("gameWindow")
    };
    game = new Phaser.Game(config);
}