<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="gas-pump" tilewidth="32" tileheight="32" tilecount="3" columns="1">
 <image source="../../textures/gas-pump.png" width="32" height="96"/>
 <tile id="0">
  <properties>
   <property name="collides" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="collides" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="collides" type="bool" value="false"/>
  </properties>
 </tile>
</tileset>
