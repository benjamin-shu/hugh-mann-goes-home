<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="sign" tilewidth="32" tileheight="32" tilecount="4" columns="2">
 <image source="../../textures/sign.png" width="64" height="64"/>
 <tile id="0">
  <properties>
   <property name="collides" type="bool" value="false"/>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="sign"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="collides" type="bool" value="false"/>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="sign"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="collides" type="bool" value="false"/>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="sign"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="collides" type="bool" value="false"/>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="sign"/>
  </properties>
 </tile>
</tileset>
