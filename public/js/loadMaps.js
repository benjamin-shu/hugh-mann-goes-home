var loadMapHandlers = {
    0 : loadMap0,
    1 : loadMap1,
    2 : loadMap2,
    3 : loadMap3,
    4 : loadMap4
}

function loadMap0() {
    console.log(this);
    /*Destroy sprites not in map*/
    while(npcs.length > 0){
        var temp = npcs.pop();
        temp.destroy();
    }
    /* Create new tilemap object. */
    map = generator.make.tilemap({ key: 'map1'});

    /*Create tilesets*/
    detail = map.addTilesetImage('details', 'detailImage');
    tiles = map.addTilesetImage('maptiles','maptiles');
    interactives = map.addTilesetImage('interactives', 'interactivesImage');

    /* Add visual details to the tilemap. */
    layerDetails = map.createStaticLayer('detail', details, 0, 0);

    /* Add interactive objects to the tilemap. */
    interactiveObjects = new Array();
    layerInteractives = map.createStaticLayer('interactives', interactives, 0, 0);

    /* Apply tileset.png to the map. */
    layerPlatforms = map.createStaticLayer('platforms', tiles, 0, 0);
    layerPlatforms.setCollisionByProperty({ collides: true });

    rock = generator.add.sprite(512,500,'rock');
    rock.name = 'rock';
    npcs.push(rock);

    player.destroy();
    var xPos = (forward) ? 17 : gameWidth - 17;
    player = generator.physics.add.sprite(xPos, 480, items.disguise ? 'hughMann-idle-disguise' : 'hughMann-idle');
    player.setCollideWorldBounds(true);
    generator.physics.add.collider(player, layerPlatforms);
}

function loadMap1() {
    console.log(this);
    /*Destroy sprites not in map*/
    while(npcs.length > 0){
        var temp = npcs.pop();
        temp.destroy();
    }
    /* Create new tilemap object. */
    map = generator.make.tilemap({ key: 'map2'});

    /*Create tilesets*/
    details = map.addTilesetImage('details', 'detailImage');
    tiles = map.addTilesetImage('maptiles','maptiles');
    interactives = map.addTilesetImage('interactives', 'interactivesImage');

    /* Add visual details to the tilemap. */
    layerDetails = map.createStaticLayer('detail', details, 0, 0);

    /* Add interactive objects to the tilemap. */
    interactiveObjects = new Array();
    layerInteractives = map.createStaticLayer('interactives', interactives, 0, 0);

    /* Apply tileset.png to the map. */
    layerPlatforms = map.createStaticLayer('platforms', tiles, 0, 0);
    layerPlatforms.setCollisionByProperty({ collides: true });

    /* Add sprites to the tilemap */
    gasMan = generator.add.sprite(900, 480, 'gas-man');
    gasMan.name = "gasMan";
    gasMan.play('idle-station-attendant-disguise');
    npcs.push(gasMan);

    player.destroy();
    var xPos = (forward) ? 17 : gameWidth - 17;
    player = generator.physics.add.sprite(xPos, 480, items.disguise ? 'hughMann-idle-disguise' : 'hughMann-idle');
    player.setCollideWorldBounds(true);
    generator.physics.add.collider(player, layerPlatforms);
}

function loadMap2() {
    console.log(this);
    /*Destroy sprites not in map*/
    while(npcs.length > 0){
        var temp = npcs.pop();
        temp.destroy();
    }

    /* Create new tilemap object. */
    map = generator.make.tilemap({ key: 'map3'});

    /*Create tilesets*/
    tiles = map.addTilesetImage('maptiles','maptiles');

    /* Apply tileset.png to the map. */
    layerPlatforms = map.createStaticLayer('platforms', tiles);
    layerPlatforms.setCollisionByProperty({ collides: true });

    /* Add visual details to the tilemap. */
    details = map.addTilesetImage('details', 'detailImage');
    layerDetails = map.createStaticLayer('detail', details, 0, 0);

    /* Add interactive objects to the tilemap. */
    interactives = map.addTilesetImage('interactives', 'interactivesImage');
    layerInteractives = map.createStaticLayer('interactives', interactives, 0, 0);

    /* Add sprites to the tilemap */
    mom = generator.add.sprite(800, 480, 'mom');
    mom.name = "mom";
    mom.play('idle-mom');
    npcs.push(mom);

    kid = generator.add.sprite(900,480,'kid');
    kid.name = "kid";
    kid.play('idle-kid');
    npcs.push(kid);

    if (!items.kite) {
        kite = generator.physics.add.sprite(920,420,'kite');
        npcs.push(kite);
    }

    player.destroy();
    var xPos = (forward) ? 17 : gameWidth - 17;
    player = generator.physics.add.sprite(xPos, 480, items.disguise ? 'hughMann-idle-disguise' : 'hughMann-idle');
    player.setCollideWorldBounds(true);
    generator.physics.add.collider(player, layerPlatforms);
    if (!items.kite) { generator.physics.add.overlap(player, kite, getKite, null, generator); }
}

function loadMap3() {
    console.log(this);
    /*Destroy sprites not in map*/
    while(npcs.length > 0){
        var temp = npcs.pop();
        temp.destroy();
    }

    /* Create new tilemap object. */
    map = generator.make.tilemap({ key: 'map4'});
    console.log(map);

    /*Create tilesets*/
    tiles = map.addTilesetImage('maptiles','maptiles');
    console.log(tiles);

    /* Apply tileset.png to the map. */
    layerPlatforms = map.createStaticLayer('platforms', tiles);
    layerPlatforms.setCollisionByProperty({ collides: true });
    console.log(layerPlatforms);

    /* Add visual details to the tilemap. */
    details = map.addTilesetImage('details', 'detailImage');
    console.log(details);
    layerDetails = map.createStaticLayer('detail', details, 0, 0);
    console.log(layerDetails);

    /* Add interactive objects to the tilemap. */
    interactives = map.addTilesetImage('interactives', 'interactivesImage');
    console.log(interactives);
    layerInteractives = map.createStaticLayer('interactives', interactives, 0, 0);
    console.log(layerInteractives);

    /* Add sprites to the tilemap */
    teacher = generator.add.sprite(800, 480, 'teacher');
    teacher.name = "teacher";
    teacher.play('idle-teacher');
    npcs.push(teacher);

    player.destroy();
    var xPos = (forward) ? 17 : gameWidth - 17;
    player = generator.physics.add.sprite(xPos, 480, items.disguise ? 'hughMann-idle-disguise' : 'hughMann-idle');
    player.setCollideWorldBounds(true);
    generator.physics.add.collider(player, layerPlatforms);
}

function loadMap4() {
    console.log(this);
    /*Destroy sprites not in map*/
    while(npcs.length > 0){
        var temp = npcs.pop();
        temp.destroy();
    }

    /* Create new tilemap object. */
    map = generator.make.tilemap({ key: 'map5'});

    /*Create tilesets*/
    tiles = map.addTilesetImage('maptiles','maptiles');

    /* Apply tileset.png to the map. */
    layerPlatforms = map.createStaticLayer('platforms', tiles);
    layerPlatforms.setCollisionByProperty({ collides: true });

    /* Add visual details to the tilemap. */
    details = map.addTilesetImage('details', 'detailImage');
    layerDetails = map.createStaticLayer('detail', details, 0, 0);

    /* Add interactive objects to the tilemap. */
    interactives = map.addTilesetImage('interactives', 'interactivesImage');
    layerInteractives = map.createStaticLayer('interactives', interactives, 0, 0);

    /* Add sprites to the tilemap */
    fastfoodGuy = generator.add.sprite(360, 480, 'fastfoodguy');
    fastfoodGuy.name = "fastfoodguy";
    fastfoodGuy.play('idle-fastfoodguy');
    npcs.push(fastfoodGuy);

    player.destroy();
    var xPos = (forward) ? 17 : gameWidth - 17;
    player = generator.physics.add.sprite(xPos, 480, items.disguise ? 'hughMann-idle-disguise' : 'hughMann-idle');
    player.setCollideWorldBounds(true);
    generator.physics.add.collider(player, layerPlatforms);
}