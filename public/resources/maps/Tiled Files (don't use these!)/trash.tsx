<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="trash" tilewidth="32" tileheight="32" tilecount="1" columns="1">
 <properties>
  <property name="collides" type="bool" value="false"/>
  <property name="interactive" type="bool" value="true"/>
  <property name="name" value="trash"/>
 </properties>
 <image source="../../textures/trash-can.png" width="32" height="32"/>
</tileset>
