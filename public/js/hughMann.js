/* Create Phaser configuration settings. */
var config;

var player;
var currentTile, moved = false, forward = true;
var cursors;

/* Variables to track items in the player's inventory. */
var items = {
    disguise    : false,
    rock        : false,
    vegOil      : false,
    kite        : false,
    dirtyMag    : false,
    tools       : false,
    starMap     : false,
    radOil      : false
};

/*Names for npcs and movable objects*/
var gasMan;
var mom;
var kid;
var kite;
var rock;
var teacher;
var fastfoodGuy;

/*Array to hold npcs and movable objects*/
var npcs = [];

var tileMaps = [];
var map, mapIndex, tiles, details, interactives, background;
var layerBackground, layerDetails, layerInteractives, layerPlatforms;
const gameWidth = 1024, gameHeight = 576;
var screenObjects, screenPeople;

var camera, controls;
var susMeter, sus = 0;
var gameOver = false, stopped = false, gameWon = false;

const tileWidth = 32;
const tileHeight = 32;

var velocity = 128;
const minSpeed = 128;
const maxSpeed = 640;

var game, generator;
var commandLine;

var splashStart = false,
    mainMenu = false,
    controls = false,
    help = false, story = false, chars = false, dev = false;
var splashScreen,
    menuScreen,
    controlsScreen,
    helpScreen, storyScreen, charsScreen, devScreen;
var winScreen, loseScreen;

/* Variables for background musics and sound effects */
var menuMusic;
var startMusic;
var objectInteractionSound;
var movingSound;
var gameOverSound;

function updateInteractionList() {
    currentTile = map.getTileAt(Math.floor(player.x / 32), Math.floor((player.y + 20) / 32), false, layerInteractives);
    /*Added this outer while loop to prevent that undefined error we kept getting*/
    while (screenObjects != null) {
        while (screenObjects.firstChild) {
            screenObjects.firstChild.remove();
        }
    }
    $('#objects').empty();
    if (currentTile != null && currentTile.properties.interactive) {
        var node = document.createElement('li');
        var text = document.createTextNode(currentTile.properties.name);
        node.appendChild(text);
        /*Added this outer while loop to prevent that undefined error we kept getting*/
        while(screenObjects != null){
            screenObjects.appendChild(node);
        }
        $('#objects').append(node);
    }
    /*Added this outer while loop to prevent that undefined error we kept getting*/
    while(screenPeople != null){
        while (screenPeople.firstChild) {
            screenPeople.firstChild.remove();
        }
    }
    $('#people').empty();
    var i = 0, bounds = player.getBounds();
    for (i = 0; i < npcs.length; i++) {
        if (Phaser.Geom.Intersects.RectangleToRectangle(bounds, npcs[i].getBounds())) {
            var node = document.createElement('li');
            var text = document.createTextNode(npcs[i].name);
            node.appendChild(text);
            /*Added this outer while loop to prevent that undefined error we kept getting*/
            while(screenPeople != null){
                screenPeople.appendChild(node);
            }
            $('#people').append(node);
        }
    }
}

function preload() {
    /* Load background image. */
    this.load.image('background', 'resources/textures/background.png');
    this.load.image('splashScreen', 'resources/menu/splash.png');
    this.load.image('menuScreen', 'resources/menu/main.png');
    this.load.image('controlsScreen', 'resources/menu/controls.png');
    this.load.image('helpScreen', 'resources/menu/help.png');
    this.load.image('storyScreen', 'resources/menu/story.png');
    this.load.image('charsScreen', 'resources/menu/characters.png');
    this.load.image('devScreen', 'resources/menu/developers.png');

    this.load.image('winScreen', 'resources/menu/victory.png');
    this.load.image('loseScreen', 'resources/menu/gameOver.png');

    /* Load sprite sheets for Hugh Mann. */
    this.load.spritesheet('hughMann-idle', 'resources/textures/hugh-idle-sheet.png', { frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('hughMann-idle-disguise', 'resources/textures/hugh-idle-disguise-sheet.png', { frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('hughMann-walking-left', 'resources/textures/hugh-walking-left-sheet.png', { frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('hughMann-walking-right', 'resources/textures/hugh-walking-right-sheet.png', { frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('hughMann-walking-left-disguise', 'resources/textures/hugh-walking-left-disguise-sheet.png', { frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('hughMann-walking-right-disguise', 'resources/textures/hugh-walking-right-disguise-sheet.png', { frameWidth: 32, frameHeight: 64 });

    /* Load sprite sheets for NPCS */
    this.load.spritesheet('gas-man','resources/textures/Station-attendant-sheet.png',{frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('gas-man-disguise','resources/textures/Station-attendant-sheet-with-disguise.png',{frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('mom','resources/textures/mom-sheet.png',{frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('kid','resources/textures/kid-sheet.png',{frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('teacher','resources/textures/teacher-sheet.png',{frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('fastfoodguy','resources/textures/fastfoodguy-sheet.png',{frameWidth: 32, frameHeight: 64 });

    /*Load sprite sheets for moving objects*/
    this.load.spritesheet('kite','resources/textures/kite.png',{frameWidth: 32, frameHeight: 64 });
    this.load.spritesheet('rock','resources/textures/rock.png',{frameWidth: 32, frameHeight: 32 });
    /* Load tileset images for the main three map layers. */
    this.load.image('maptiles', 'resources/textures/maptiles.png');
    this.load.image('detailImage', 'resources/textures/details.png');
    this.load.image('interactivesImage', 'resources/textures/interactives.png');

    /*Load map info*/
    tileMaps.push(this.load.tilemapTiledJSON('map1', 'resources/maps/map1.json'));
    tileMaps.push(this.load.tilemapTiledJSON('map2', 'resources/maps/map2.json'));
    tileMaps.push(this.load.tilemapTiledJSON('map3', 'resources/maps/map3.json'));
    tileMaps.push(this.load.tilemapTiledJSON('map4', 'resources/maps/map4.json'));
    tileMaps.push(this.load.tilemapTiledJSON('map5', 'resources/maps/map5.json'));
    mapIndex = 0;

    /* Load music */
    this.load.audio('firstAreaMusic','resources/audio/startingMusic.mp3');
    this.load.audio('gameMenuMusic', 'resources/audio/GameMenuMusic.mp3');
    this.load.audio('wheelSound', 'resources/audio/wheelSound.mp3');
    this.load.audio('objectInteractionSound', 'resources/audio/objectInteractionSound.mp3');
    this.load.audio('gameOverSound', 'resources/audio/gameOverSound.wav');
};

function create() {
    generator = this;
    /* Set up backgrround musics */
    startMusic = this.sound.add('firstAreaMusic', {loop: true});
    menuMusic = this.sound.add('gameMenuMusic', {loop: true});
    movingSound = this.sound.add('wheelSound', {loop: true, volume: 0.2});
    objectInteractionSound = this.sound.add('objectInteractionSound');
    gameOverSound = this.sound.add('gameOverSound');

    /* Configure music to loop */
    menuMusic.play();

    /* Load background image. */
    this.add.image(1024, 270,'background');

    /* Create new tilemap object. */
    map = this.make.tilemap({ key: 'map1'});

    /*Create tilesets*/
    detail = map.addTilesetImage('details', 'detailImage');
    tiles = map.addTilesetImage('maptiles','maptiles');
    interactives = map.addTilesetImage('interactives', 'interactivesImage');

    /* Add visual details to the tilemap. */
    layerDetails = map.createStaticLayer('detail', detail, 0, 0);

    /* Add interactive objects to the tilemap. */
    interactiveObjects = new Array();
    layerInteractives = map.createStaticLayer('interactives', interactives, 0, 0);

    /* Apply tileset.png to the map. */
    layerPlatforms = map.createStaticLayer('platforms', tiles, 0, 0);
    layerPlatforms.setCollisionByProperty({ collides: true });
    
    rock = generator.add.sprite(512,500,'rock');
    rock.name = 'rock';
    npcs.push(rock);

    /* Add Hugh Mann sprite for player to use. */
    player = this.physics.add.sprite(256, 480, 'hughMann-idle');
    player.setCollideWorldBounds(true);
    this.physics.add.collider(player, layerPlatforms);
    
    /* Add animations to Hugh Mann sprite. */
    this.anims.create({
        key: 'left',
        frames: this.anims.generateFrameNumbers('hughMann-walking-left', { start: 0, end: 6 }),
        frameRate: 16,
        repeat: 0
    });

    this.anims.create({
        key: 'left-disguise',
        frames: this.anims.generateFrameNumbers('hughMann-walking-left-disguise', { start: 0, end: 6 }),
        frameRate: 16,
        repeat: 0
    });

    this.anims.create({
        key: 'right',
        frames: this.anims.generateFrameNumbers('hughMann-walking-right', { start: 0, end: 6 }),
        frameRate: 16,
        repeat: 0
    });

    this.anims.create({
        key: 'right-disguise',
        frames: this.anims.generateFrameNumbers('hughMann-walking-right-disguise', { start: 0, end: 6 }),
        frameRate: 16,
        repeat: 0
    });

    this.anims.create({
        key: 'idle',
        frames: this.anims.generateFrameNumbers('hughMann-idle', { start: 0, end: 26 }),
        frameRate: 4,
        repeat: -1
    });

    this.anims.create({
        key: 'idle-disguise',
        frames: this.anims.generateFrameNumbers('hughMann-idle-disguise', { start: 0, end: 26 }),
        frameRate: 4,
        repeat: -1
    });

    /* Add animations for NPCS */
    this.anims.create({
        key: 'idle-station-attendant',
        frames: this.anims.generateFrameNumbers('gas-man', { start: 0, end: 16 }),
        frameRate: 4,
        repeat: -1
    });
    this.anims.create({
        key: 'idle-station-attendant-disguise',
        frames: this.anims.generateFrameNumbers('gas-man-disguise', { start: 0, end: 16 }),
        frameRate: 4,
        repeat: -1
    });
    this.anims.create({
        key: 'idle-mom',
        frames: this.anims.generateFrameNumbers('mom', { start: 0, end: 16 }),
        frameRate: 6,
        repeat: -1
    });
    this.anims.create({
        key: 'idle-kid',
        frames: this.anims.generateFrameNumbers('kid', { start: 0, end: 16 }),
        frameRate: 7,
        repeat: -1
    });
    this.anims.create({
        key: 'idle-teacher',
        frames: this.anims.generateFrameNumbers('teacher', { start: 0, end: 16 }),
        frameRate: 7,
        repeat: -1
    });
    this.anims.create({
        key: 'idle-fastfoodguy',
        frames: this.anims.generateFrameNumbers('fastfoodguy', {start:0, end: 16}),
        frameRate: 7,
        repeat: -1
    });

    /* Set up to detect keyboard input. */
    cursors = this.input.keyboard.addKeys({
        left    : Phaser.Input.Keyboard.KeyCodes.LEFT,
        right   : Phaser.Input.Keyboard.KeyCodes.RIGHT,
        enter   : Phaser.Input.Keyboard.KeyCodes.ENTER,
        key1    : Phaser.Input.Keyboard.KeyCodes.ONE,
        key2    : Phaser.Input.Keyboard.KeyCodes.TWO,
        key3    : Phaser.Input.Keyboard.KeyCodes.THREE,
        keyB    : Phaser.Input.Keyboard.KeyCodes.B
    });

    camera = this.cameras.main;
    camera.width = 1024;
    camera.height = 576;
    camera.setBounds(0, 0, 1024, 576);
    camera.scrollX = 0; camera.scrollY = 0;
    camera.startFollow(player);

    menuScreen = this.add.image(512, 288, 'menuScreen');
    splashScreen = this.add.image(512, 288, 'splashScreen');
};

var key3Lock = false, keyBLock = false;

function update(){
    if (!splashStart) {
        if (cursors.enter.isDown) {
            splashStart = true;
            splashScreen.destroy();
            mainMenu = true;
        }
    } else if (mainMenu) {
        if (cursors.key1.isDown) {
            mainMenu = false;
            generator.input.keyboard.removeCapture(Phaser.Input.Keyboard.KeyCodes.B);
            menuMusic.stop();
            startMusic.play();
            menuScreen.destroy();
        } else if (cursors.key2.isDown) {
            mainMenu = false;
            controls = true;
            controlsScreen = this.add.image(512, 288, 'controlsScreen');
        } else if (cursors.key3.isDown) {
            key3Lock = true;
            mainMenu = false;
            help = true;
            helpScreen = this.add.image(512, 288, 'helpScreen');
        }
    } else if (controls) {
        if (cursors.keyB.isDown) {
            mainMenu = true;
            controls = false;
            controlsScreen.destroy();
        }
    } else if (help) {
        if (cursors.key1.isDown) {
            help = false;
            story = true;
            storyScreen = this.add.image(512, 288, 'storyScreen');
        } else if (cursors.key2.isDown) {
            help = false;
            chars = true;
            charsScreen = this.add.image(512, 288, 'charsScreen');
        } else if (cursors.key3.isDown && !key3Lock) {
            key3Lock = true;
            help = false;
            dev = true;
            devScreen = this.add.image(512, 288, 'devScreen');
        } else if (cursors.keyB.isDown && !keyBLock) {
            keyBLock = true;
            help = false;
            mainMenu = true;
            helpScreen.destroy();
        }
        if (cursors.keyB.isUp) { keyBLock = false; }
        if (cursors.key3.isUp) { key3Lock = false; }
    } else if (story) {
        if (cursors.keyB.isDown && !keyBLock) {
            keyBLock = true;
            story = false;
            help = true;
            storyScreen.destroy();
        }
        if (cursors.keyB.isUp) { keyBLock = false; }
        if (cursors.key3.isUp) { key3Lock = false; }
    } else if (chars) {
        if (cursors.keyB.isDown && !keyBLock) {
            keyBLock = true;
            chars = false;
            help = true;
            charsScreen.destroy();
        }
        if (cursors.keyB.isUp) { keyBLock = false; }
        if (cursors.key3.isUp) { key3Lock = false; }
    } else if (dev) {
        if (cursors.keyB.isDown && !keyBLock) {
            keyBLock = true;
            dev = false;
            help = true;
            devScreen.destroy();
        }
        if (cursors.keyB.isUp) { keyBLock = false; }
        if (cursors.key3.isUp) { key3Lock = false; }
    } else if (gameWon) {
        if (stopped) { return; }
        else {
            stopped = true;
            winScreen = generator.add.image(512, 288, 'winScreen');
        }
    } else if (gameOver) {
        if (stopped) { return; }
        else {
            stopped = true;
            loseScreen = generator.add.image(512, 288, 'loseScreen');
            
            // loseScreen.alpha = 0;
            // var tween = generator.tweens.add({
            //     targets     : loseScreen,
            //     alpha       : 1,
            //     ease        : 'Linear',         // 'Cubic', 'Elastic', 'Bounce', 'Back'
            //     duration    : 5000,
            //     repeat      : 0,                // -1: infinity
            //     yoyo        : false
            // });
            // tween.play();
            // gameOverSound.play();
        }
    } else {
        updateInteractionList();
        if (cursors.left.isDown) {
            if (!prompted) {
                forward = false;
                if (player.x == 16 && mapIndex > 0) {
                    mapIndex--;
                    console.log("new map index is " + mapIndex);
                    map.destroy();
                    loadMapHandlers[mapIndex]();
                }
                player.setVelocityX(velocity * -1);
                player.anims.play(items.disguise ? 'left-disguise' : 'left', true);
                moved = true;
            }
        } else if (cursors.right.isDown) {
            if (!prompted) {
                forward = true;
                if (player.x == 1008 && mapIndex < tileMaps.length - 1) {
                    mapIndex++;
                    console.log("new map index is " + mapIndex);
                    map.destroy();
                    loadMapHandlers[mapIndex]();
                }
                player.setVelocityX(velocity * 1);
                player.anims.play(items.disguise ? 'right-disguise' : 'right', true);
                moved = true;
            }
        } else {
            player.setVelocityX(0);
            player.anims.play(items.disguise ? 'idle-disguise' : 'idle', true);
            if (moved) {
                moved = false;
            }
        }
        // if (cursors.key1.isDown) {
        //     /* Teleport to Area 1 - Tutorial. */
        //     /*Destroy sprites not in map*/
        //     while(npcs.length > 0){
        //         var temp = npcs.pop();
        //         temp.destroy();
        //     }
                        
        //     map.destroy();
        //     mapIndex = 0;
        //     loadMap0();
        // } else if (cursors.key2.isDown) {
        //     /* Teleport to Area 2 - Gas Station. */
        //     /*Destroy sprites not in map*/
        //     while(npcs.length > 0){
        //         var temp = npcs.pop();
        //         temp.destroy();
        //     }
        //     map.destroy();
        //     mapIndex = 1;
        //     loadMap1();
        // } else if (cursors.key3.isDown) {
        //     /* Teleport to Area 3 - Park. */
        //     /*Destroy sprites not in the map*/
        //     /*Destroy sprites not in map*/
        //     while(npcs.length > 0){
        //         var temp = npcs.pop();
        //         temp.destroy();
        //     }
        //     map.destroy();
        //     mapIndex = 2;
        //     loadMap2();
        // }
    }
    if (player.body.speed > 0 && splashStart == true) {
        if (movingSound.isPlaying == false) {
            movingSound.play();
        }
    } else {
        movingSound.stop();
    }
};

/* Create new game object. */
window.onload = function() {
    commandLine     = document.getElementById("commandLine");
    screenObjects   = document.getElementById("objects");
    screenPeople    = document.getElementById("people");
    susMeter        = document.getElementById("suspicion");
    susMeter.innerHTML = sus + " %";

    config = {
        type: Phaser.CANVAS,
        width: gameWidth,
        height: gameHeight,
        physics: {
            default: 'arcade',
            arcade: {
                gravity: { y: 0 },
                debug: false
            }
        },
        scene: {
            preload: preload,
            create: create,
            update: update
        },
        canvas: document.getElementById("gameWindow")
    };
    game = new Phaser.Game(config);
}