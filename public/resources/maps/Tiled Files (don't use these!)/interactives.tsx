<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="interactives" tilewidth="32" tileheight="32" tilecount="128" columns="8">
 <image source="../../textures/interactives.png" width="256" height="516"/>
 <tile id="0">
  <properties>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="swing"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="swing"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="sign"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="sign"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="ufo"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="ufo"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="swing"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="swing"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="sign"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="sign"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="ufo"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="ufo"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="tree"/>
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="tree"/>
  </properties>
 </tile>
 <tile id="32">
  <properties>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="tree"/>
  </properties>
 </tile>
 <tile id="40">
  <properties>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="tree"/>
  </properties>
 </tile>
</tileset>
