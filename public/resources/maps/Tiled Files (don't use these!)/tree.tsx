<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="tree" tilewidth="32" tileheight="32" tilecount="4" columns="1">
 <image source="../../textures/tree.png" width="32" height="128"/>
 <tile id="0">
  <properties>
   <property name="collides" type="bool" value="false"/>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="tree"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="collides" type="bool" value="false"/>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="tree"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="collides" type="bool" value="false"/>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="tree"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="collides" type="bool" value="false"/>
   <property name="interactive" type="bool" value="true"/>
   <property name="name" value="tree"/>
  </properties>
 </tile>
</tileset>
