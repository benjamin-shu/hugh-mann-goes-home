var historyIndex = 0;

/* Text window here! */
$(document).ready(function() {
    var inputHistory = [];

    localStorage.setItem('history', JSON.stringify(inputHistory));

    $("body").keydown(function(event) {
        if (event.key == "Enter") {
            var text = $("#text_input").val();
            if (text != "") {
                $("#history").append("<p class='history_p'>" + text + "</p>");
                inputHistory.push(text);
                historyIndex = inputHistory.length - 1;
            }
        }
        if (event.keyCode == 38) {
            if (inputHistory.length > 0) {
                $("#text_input").val(inputHistory[historyIndex--]);
            }
        }
        if (event.keyCode == 40) {
            if (historyIndex < inputHistory.length - 1) {
                $("#text_input").val(inputHistory[++historyIndex]);
            }
        }
    });
});